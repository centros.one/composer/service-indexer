# Service Indexer

## Requirements
- Python 3.7+
- Helm (https://helm.sh/docs/intro/install/)
- Python dependencies (`pip3 install requirements.txt`, venv recommended)
