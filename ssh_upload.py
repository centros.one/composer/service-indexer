import os
from stat import S_ISDIR

from paramiko import SSHClient, RSAKey, AutoAddPolicy, SFTPClient
from scp import SCPClient


def rm(sftp, path, contents_only=False):
    files = sftp.listdir(path)
    for f in files:
        subpath = os.path.join(path, f)
        if S_ISDIR(sftp.stat(subpath).st_mode):
            rm(sftp, subpath)
        else:
            sftp.remove(subpath)
    if not contents_only:
        sftp.rmdir(path)


def do_upload(ssh_server, ssh_port, ssh_upload_path, ssh_user, ssh_key_file, ssh_upload_method, source):
    with SSHClient() as ssh:
        pkey = RSAKey.from_private_key_file(ssh_key_file)
        ssh.set_missing_host_key_policy(AutoAddPolicy)
        ssh.connect(ssh_server, port=ssh_port, username=ssh_user, pkey=pkey)

        if ssh_upload_method == "scp":
            # remove the content of the upload folder before uploading the new artifacts
            ssh_stdin, _, _ = ssh.exec_command(f"rm -rf {ssh_upload_path}/*")

            # for the reasoning behind the following line refer to https://stackoverflow.com/questions/37556888
            # and no, just using _ in place of ssh_stdin does not seem to work for some reason
            del ssh_stdin

            with SCPClient(ssh.get_transport()) as scp:
                scp.put([os.path.join(source, f) for f in os.listdir(source)], ssh_upload_path, recursive=True)
        else:
            with ssh.open_sftp() as sftp:  # type: SFTPClient
                rm(sftp, ssh_upload_path, contents_only=True)
                os.chdir(source)
                for walker in os.walk("."):
                    try:
                        if walker[0] != ".":
                            sftp.mkdir(os.path.normpath(os.path.join(ssh_upload_path, walker[0])))
                    except IOError:
                        pass
                    for file in walker[2]:
                        sftp.put(
                            os.path.normpath(os.path.join(walker[0], file)),
                            os.path.normpath(os.path.join(ssh_upload_path, walker[0], file)))
