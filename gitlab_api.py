import logging
import urllib.request
import urllib.parse
import json
from urllib.error import HTTPError


class Api:
    def _get(self, path):
        url = f"{self.baseurl}/{path if path[0] != '/' else path[1:]}"
        try:
            req = urllib.request.Request(url, headers={"PRIVATE-TOKEN": self.token})
            return json.loads(urllib.request.urlopen(req).read())
        except HTTPError as err:
            self.logger.error(f"Error reading {url}: {str(err)}")
            return None

    def _get_raw(self, path):
        path = path if path[0] != '/' else path[1:]
        req = urllib.request.Request(
            f"{self.baseurl}/{path}",
            headers={"PRIVATE-TOKEN": self.token}
        )
        return urllib.request.urlopen(req).read().decode('utf-8')

    def __init__(self, baseurl, token):
        self.baseurl = baseurl
        self.token = token
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def urlencode(param):
        # param may be a string identifying a group/project/tag by its path, or an int with the actual ID
        return urllib.parse.quote(param, safe='') if type(param) == str else param

    def get_projects_for_group(self, group):
        return self._get(f"/groups/{Api.urlencode(group)}/projects")

    def get_tags_for_project(self, project):
        return self._get(f"projects/{Api.urlencode(project)}/repository/tags")

    def get_project(self, project):
        return self._get(f"projects/{Api.urlencode(project)}")

    def get_file_content(self, project, file, commit_id):
        return self._get_raw(
            f"projects/{Api.urlencode(project)}/repository/files/{Api.urlencode(file)}/raw?ref={commit_id}"
        )

    def get_repo_root(self, project, tag_name):
        # TODO: use pagination if there should ever be more than 100 files in the root folder of a project
        # (100 is the maximum allowed by gitlab)
        return self._get(f"projects/{Api.urlencode(project)}/repository/tree?ref={tag_name}&per_page=100")
