FROM python:3.7

# first two commands add signing key & repo necessary for Helm package installation
RUN curl https://baltocdn.com/helm/signing.asc | apt-key add - \
 && echo "deb https://baltocdn.com/helm/stable/debian/ all main" > /etc/apt/sources.list.d/helm-stable-debian.list \
 && apt-get update \
 && apt-get install -y --no-install-recommends apt-transport-https gosu git helm \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

ARG COMMIT_SHA
ENV COMMIT_SHA=${COMMIT_SHA}
ARG COMMIT_REF_SLUG
ENV COMMIT_REF_SLUG=${COMMIT_REF_SLUG}

COPY . /usr/src/app

RUN mkdir /tmp/indexer
ENV INDEXER_WORK_FOLDER=/tmp/indexer

CMD ["python3", "build_index.py", "--help"]
