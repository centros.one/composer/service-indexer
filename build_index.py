import argparse
import hashlib
import json
import logging
import os
import pathlib
import re
import shutil
import subprocess
import urllib.parse
import urllib.request
from datetime import datetime, timezone

import jsonschema
import requests
import ruamel.yaml as yaml
import semver
from requests import RequestException

import gitlab_api
from ssh_upload import do_upload

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("build_index")


def add_arg(p, name, envvar, description=None, default=None, choices=None):
    defval = os.environ.get(envvar)
    description = "" if not description else description + "\n"
    description += "Equivalent env-var: " + envvar
    if defval:
        description += "\nCurrent value: " + defval
    else:
        defval = default
    if default:
        description += "\nDefault value: " + str(default)
    p.add_argument("--" + name, required=not defval, default=defval, help=description, choices=choices)


# noinspection PyTypeChecker
parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                                 description="All parameters can alternatively be provided via environment variables.")
add_arg(parser, "group-paths", "INDEXER_GROUP_PATHS", "Paths to project group containing services (separated by ';')")
add_arg(parser, "work-folder", "INDEXER_WORK_FOLDER", "Path to folder for temporarily storing source & build artefacts")
add_arg(parser, "build-user", "INDEXER_BUILD_USER", "Username for accessing gitlab")
add_arg(parser, "build-user-token", "INDEXER_BUILD_USER_TOKEN", "Private auth token for the build user")
add_arg(parser, "ssh-server", "INDEXER_SSH_SERVER", "Address of the server the finished indes is uploaded to")
add_arg(parser, "ssh-port", "INDEXER_SSH_PORT", default=22)
add_arg(parser, "ssh-upload-path", "INDEXER_SSH_UPLOAD_PATH", "Path on the server to upload to")
add_arg(parser, "ssh-upload-method", "INDEXER_SSH_UPLOAD_METHOD", "Specify method to use for upload",
        choices=["scp", "sftp"], default="scp")
add_arg(parser, "ssh-user", "INDEXER_SSH_USER", "Username for authentication for the upload")
add_arg(parser, "ssh-key-file", "INDEXER_SSH_KEY_FILE", "Private key file for authentication")
args = parser.parse_args()

build_folder = os.path.abspath(args.work_folder) + "/indexer/build"
output_folder = os.path.abspath(args.work_folder) + "/indexer/out"

api = gitlab_api.Api("https://gitlab.com/api/v4", args.build_user_token)

# just in case the output folder already exists: delete and recreate it to remove possible leftovers from a previous run
if os.path.exists(output_folder):
    shutil.rmtree(output_folder)
os.makedirs(output_folder)

# Pattern recommendation as per https://semver.org/
semver_pattern = re.compile(
    "^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)"
    "(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?"
    "(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$"
)

# CSM file name pattern: .csm[-variant].json
csm_file_pattern = re.compile(r"^\.csm(?:-([0-9a-zA-Z]+))?\.json$")
# DDD file name pattern: .ddd.json
ddd_file_pattern = re.compile(r"^\.ddd\.json$")

groups = {}
for group_path in args.group_paths.split(";"):
    # Retrieve all projects in the target group
    logger.info(f"Fetching projects for group '{group_path}'")
    groups[group_path] = api.get_projects_for_group(group_path)

cscf_schema_url = "https://gitlab.com/centros.one/composer/schemas/-/raw/0.1.0/cscf-schema.json"

# basic structure for the resulting CSCF file
cscf = {
    "$schema": cscf_schema_url,
    "services": []
}

csm_schema_whitelist = {
    "https://gitlab.com/centros.one/composer/schemas/-/raw/0.1.0/csm-schema.json",
    "https://gitlab.com/centros.one/composer/schemas/-/raw/0.1.1/csm-schema.json"
}

ddd_schema_whitelist = {
    "https://gitlab.com/centros.one/composer/schemas/-/raw/0.1.1/ddd-schema.json"
}


def checkout_tag(project, tag, target_folder_suffix: str = None):
    """
    Checkout the given project at the given tag into the build folder (including submodules)
    :param project: The project to check out
    :param tag: Check out the project at this tag
    :param target_folder_suffix: optional suffix to add to the destination folder
    :return: the destination folder on success (None on failure)
    """
    repo_url = project['http_url_to_repo']

    # inject credentials into URL
    repo_url = repo_url.replace("://", f"://{args.build_user}:{args.build_user_token}@")

    target_folder = project['path']
    if target_folder_suffix is not None:
        target_folder = target_folder + target_folder_suffix

    logger.info(f"Cloning project '{project['name_with_namespace']}' into {target_folder}")
    gitcmd = subprocess.Popen(["git", "clone", "-n", repo_url, target_folder], cwd=build_folder)
    gitcmd.wait()
    if gitcmd.returncode != 0:
        logger.error(f"Git clone failed with exit code {gitcmd.returncode}")
        return None

    project_folder = os.path.join(build_folder, target_folder)

    logger.info(f"Checking out version tagged {tag['name']}")
    gitcmd = subprocess.Popen(["git", "checkout", f"tags/{tag['name']}"], cwd=project_folder)
    gitcmd.wait()
    if gitcmd.returncode != 0:
        logger.error(f"Git checkout failed with exit code {gitcmd.returncode}")
        return None

    logger.info("Initializing submodules")
    gitcmd = subprocess.Popen(["git", "submodule", "update", "--init"], cwd=project_folder)
    gitcmd.wait()
    if gitcmd.returncode != 0:
        logger.error(f"Git submodule update failed with exit code {gitcmd.returncode}")
        return None

    return project_folder


def validate_csm(csm):
    return validate_json(csm, csm_schema_whitelist, "CSM")


def validate_ddd(ddd):
    return validate_json(ddd, ddd_schema_whitelist, "DDD")


def validate_json(file_path, schema_whitelist, file_type):
    with open(file_path) as f:
        data = json.load(f)

    schema_url = data['$schema']
    if not schema_url:
        logger.error(f"{file_type} file '{file_path}' has no schema")
        return False
    if schema_url not in schema_whitelist:
        logger.error(f"{file_type} file '{file_path}' has unknown schema '{schema_url}'")
        return False

    schema = {"$ref": schema_url}
    try:
        jsonschema.validate(data, schema)
        return True
    except jsonschema.exceptions.ValidationError as ex:
        logger.error(f"{file_type} file '{file_path}' is not valid: {str(ex)}")
        return False


def find_csm_files(path):
    """
    Get full paths to all files in a given directory with names matching the CSM filename pattern
    and whose contents also pass validation against the CSM schema.
    :param path: the directory to look for files in
    :return: list of paths to valid CSM files
    """
    matched = [os.path.join(path, f) for f in os.listdir(path) if csm_file_pattern.match(f)]
    return [f for f in matched if validate_csm(f)]


def find_ddd_file(path):
    """
    Get full paths to all files in a given directory with names matching the DDD filename pattern
    and whose contents also pass validation against the DDD schema.
    :param path: the directory to look for files in
    :return: list of paths to valid DDD files
    """
    matched = [os.path.join(path, f) for f in os.listdir(path) if ddd_file_pattern.match(f)]
    if len(matched) > 1:
        logger.warning(f"Expected at most 1 ddd file, found {len(matched)}")
    return next(filter(validate_ddd, matched), None)


def get_file_hash(file):
    """
    Calculate MD5 checksum of a file
    :param file: the file to hash
    :return: the MD5 digest as a hexadecimal string
    """
    md5 = hashlib.md5()
    with open(file, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            md5.update(chunk)
    return md5.hexdigest()


def resolve_uri(service_sources, schema_uri):
    """
    Provide a canonical uri for the schema file by either returing a file-uri pointing to the schema file in the local
    file system (if the schema uri was relative) or returning the schema uri itself (if it was absolute)
    :param service_sources: the source folder (where the schema file should reside if the uri to is is relative)
    :param schema_uri: the schema uri given by the CSM
    :return: an absolute uri to the schema file
    """
    # convert the source path into a "file://..." uri
    file_uri = pathlib.Path(service_sources).as_uri()

    # make sure the path ends with a trailing path separator, since it's a directory (otherwise the last path segment
    # will be overwritten in the join)
    file_uri = file_uri if file_uri[-1] == "/" else file_uri + "/"

    # if the schema uri is relative, this will give us a file-uri to it
    # if it is an (absolute) http-uri, we will get that instead
    return urllib.parse.urljoin(file_uri, schema_uri)


def fetch_file(uri, destination):
    """
    Retrieve the file given by the URI either by copying it from the local file system (if the uri is a file-uri)
    or by fetching it remotely.
    :param uri: the uri to the file
    :param destination: the destination folder for storing the file
    :return: the path the file has been placed at
    """
    parsed = urllib.parse.urlparse(uri)
    if parsed.scheme == "file":
        local_path = urllib.parse.unquote(urllib.parse.urlparse(uri).path)
        return shutil.copy(local_path, destination)
    else:
        target = os.path.join(destination, os.path.basename(parsed.path))
        req = requests.get(uri)
        with open(target, 'wb') as out:
            out.write(req.content)
        return target


def copy_schemas(service_project, service_tag, service_sources, csm_file):
    """
    Copy schema files to the output folder; giving them unique filenames by adding the MD5 hash
    :param service_project: the project containing the service
    :param service_tag: the tag of the project being processed
    :param service_sources: location of the source files for the service
    :param csm_file: path to the CSM file
    :return: a dict mapping the (original) schema uri to a schema definition containing the new path/filename of
    schema file (relative to the index file)
    """
    with open(csm_file) as f:
        parsed = json.load(f)

    schema_map = {}

    for s in parsed['sockets']['input']:
        schema_map[s['keySchema']['uri']] = s['keySchema']
        schema_map[s['valueSchema']['uri']] = s['valueSchema']

    for s in parsed['sockets']['output']:
        schema_map[s['keySchema']['uri']] = s['keySchema']
        schema_map[s['valueSchema']['uri']] = s['valueSchema']

    dst = os.path.join(output_folder, service_project['path'], service_tag['name'])
    for u, s in schema_map.items():
        src = resolve_uri(service_sources, s['uri'])
        local = fetch_file(src, dst)
        md5 = get_file_hash(local)

        # inject the MD5 hash into the filename
        split = os.path.splitext(os.path.basename(local))
        final = os.path.join(os.path.dirname(local), f"{split[0]}-{md5}{split[1]}")

        # rename the file
        os.rename(local, final)

        # get the destination path relative to the output folder (which is the path relative to the index!)
        rel_path = os.path.relpath(final, output_folder)

        # update the path in the schema definition
        schema_map[u]['uri'] = rel_path
    return schema_map


def create_helm_archive(service_project, service_tag, helmchart_project, helmchart_tag, helmchart_sources):
    """
    Use the 'helm package' command to create an archive containing the helm chart
    :param service_project: the project for the service
    :param service_tag: the tag of the project
    :param helmchart_project: the project for the helm chart
    :param helmchart_tag: the tag of the helm chart project
    :param helmchart_sources: the source folder for the helm chart project
    :return: path to the created archive (None in case of an error)
    """
    archive_name = f"{helmchart_project['path']}-{helmchart_tag['name']}.tgz"
    destination_dir = os.path.join(output_folder, service_project['path'], service_tag['name'])
    destination = os.path.join(destination_dir, archive_name)
    if os.path.exists(destination):
        # Archive already exists (from two different service versions using the same helm chart version)
        return destination

    logger.info(f"Creating archive of helm chart {helmchart_project['name']}, version {helmchart_tag['name']}")
    helmcmd = subprocess.Popen(["helm", "package", "-d", destination_dir, helmchart_sources])
    helmcmd.wait()
    if helmcmd.returncode != 0:
        logger.error(f"Helm package failed with exit code {helmcmd.returncode}")
        return None

    return destination


def substitute_schemas(socket_def, schema_map):
    """
    Replace the schemas in the socket definition with the ones from the schema map containing the proper paths
    relative to the index
    :param socket_def: the socket definition object
    :param schema_map: the dict containing the map from original uri to schema with updated path
    :return:
    """
    socket_def['keySchema'] = schema_map[socket_def['keySchema']['uri']]
    socket_def['valueSchema'] = schema_map[socket_def['valueSchema']['uri']]


def create_service_version_entry(service_tag, csm_file, schema_map, helm_archive):
    """
    Create an entry for the index containing all the relevant information describing the service
    :param service_tag: the tag of the service project being processed
    :param csm_file: path to the metadata file
    :param schema_map: the dict containing the updated schemas (mapped to the original schema uri)
    :param helm_archive: the path to the archived helm chart
    :return: the data structure describing the service
    """
    # basic structure for the entry (basically, everything not already covered by the CSM)
    entry = {
        "version": service_tag['name'],
        "deploymentDescriptors": [
            {
                "type": "helm",
                "chart": os.path.relpath(helm_archive, output_folder)
            }
        ]
    }

    with open(csm_file) as f:
        parsed = json.load(f)

    # we don't want the "$schema" property from the CSM to appear in the CSCF
    if '$schema' in parsed:
        del parsed['$schema']

    # update the schema references to use the correct paths/filenames
    for s in parsed['sockets']['input']:
        substitute_schemas(s, schema_map)
    for s in parsed['sockets']['output']:
        substitute_schemas(s, schema_map)

    # include the (modified) CSM in the entry
    entry.update(parsed)

    return entry


def copy_helm_archive_from_ddd(ddd, service_project, service_tag):
    with open(ddd) as f:
        descriptor = json.load(f)
    helm_descriptor = next(filter(lambda x: x['type'] == 'helm', descriptor['deploymentDescriptors']), None)
    if helm_descriptor is not None:
        chart_uri = helm_descriptor['chart']
        destination_dir = pathlib.Path(output_folder, service_project['path'], service_tag['name'])
        destination_dir.mkdir(parents=True, exist_ok=True)
        try:
            result_file = fetch_file(chart_uri, destination_dir)
            return result_file
        except RequestException as e:
            logger.error(f'Could not copy helm chart from "{chart_uri}": {e}')
    else:
        logger.warning('No helm deployment descriptor found')
    return None


def build_index(service_project, service_tag, helmchart_project, helmchart_tag):
    """
    Create index entry for the given service and add it to the CSCF
    :param service_project: the project for the service
    :param service_tag: the tag identifying the version of the project
    :param helmchart_project: the helmchart associated with this service
    :param helmchart_tag: the tag of the helmchart
    """

    if helmchart_tag is None:
        logger.info(f"No matching Helm chart repo for service version '{service_tag['name']}', looking for ddd file")
    else:
        logger.info(f"Found Helm chart tagged as '{helmchart_tag['name']}' for service version '{service_tag['name']}'")

    # delete and recreate the output folder if necessary to eliminate leftover files
    if os.path.exists(build_folder):
        shutil.rmtree(build_folder)
    os.makedirs(build_folder)

    service_sources = checkout_tag(service_project, service_tag)
    if service_sources is None:
        return

    csms = find_csm_files(service_sources)
    if not csms:
        logger.warning(f"No CSM files found in project {service_project['name']}")
        return

    if helmchart_tag is None:
        ddd = find_ddd_file(service_sources)
        if ddd is None:
            logger.warning("No ddd file found in repo")
            return
        helm_archive = copy_helm_archive_from_ddd(ddd, service_project, service_tag)
        if helm_archive is None:
            return
    else:
        helmchart_sources = checkout_tag(helmchart_project, helmchart_tag, target_folder_suffix="-helm")
        if helmchart_sources is None:
            return

        helm_archive = create_helm_archive(service_project,
                                           service_tag,
                                           helmchart_project,
                                           helmchart_tag,
                                           helmchart_sources)

    # note that there may be multiple CSM files, describing different variants of the service
    for c in csms:
        service_type_name = service_project['path']
        csm_variant = csm_file_pattern.match(os.path.basename(c)).group(1)
        if csm_variant:
            service_type_name = service_type_name + "-" + csm_variant

        cscf_service_entry = next((s for s in cscf['services'] if s.get('type') == service_type_name), None)
        if cscf_service_entry is None:
            cscf_service_entry = {
                "type": service_type_name,
                "versions": []
            }
            cscf['services'].append(cscf_service_entry)

        schema_map = copy_schemas(service_project, service_tag, service_sources, c)
        cscf_service_entry['versions'].append(
            create_service_version_entry(service_tag, c, schema_map, helm_archive))


def upload_index():
    """
    Upload contents of the output folder to a remote location via SCP
    """
    do_upload(args.ssh_server,
              args.ssh_port,
              args.ssh_upload_path,
              args.ssh_user,
              args.ssh_key_file,
              args.ssh_upload_method,
              output_folder)


def is_valid_tag(service, tag):
    """
    Check whether the tag is a valid version tag: it matches the SemVer pattern, and the repo contains a CSM file
    :param service: the service project
    :param tag: the tag to examine
    :return: True if the tag is valid for processing, False otherwise
    """
    # tag has to match the semver pattern
    if not semver_pattern.match(tag['name']):
        logger.info(f"Tag '{tag['name']}' of service '{service['name']}' does not match SemVer pattern")
        return False

    # check repo for existence of a CSM file
    files = api.get_repo_root(service['id'], tag['name'])
    if files:
        return any(csm_file_pattern.match(file['name']) for file in files)

    logger.info(f"Tag '{tag['name']}' of service '{service['name']}' has no metadata")
    return False


def main():
    for group_path, projects in groups.items():
        for service in projects:
            project_id = service['id']
            logger.info(f"Processing service project '{service['name']}' (ID {project_id})")

            logger.info("Looking for tags with metadata (CSM)")
            all_tags = api.get_tags_for_project(project_id)
            tags = []
            if all_tags:
                for tag in all_tags:
                    if is_valid_tag(service, tag):
                        logger.info(f"Metadata found for tag '{tag['name']}' of service '{service['name']}'")
                        tags.append(tag)

            if not tags:
                logger.warning(f"Service 'service {service['name']}' has no valid tags - skipping")
                continue

            helm_path = f"{group_path}/helm-charts/{service['path']}"
            logger.info(f"Looking for Helm chart at path '{helm_path}'")
            helm_chart = api.get_project(helm_path)

            chart_map = {}
            if helm_chart is not None:
                logger.info("Looking for tags on Helm chart")
                helm_tags = api.get_tags_for_project(helm_chart['id'])

                for h in helm_tags:
                    chart_yaml = api.get_file_content(helm_chart['id'], "Chart.yaml", h['commit']['id'])
                    parsed = yaml.safe_load(chart_yaml)
                    # NOTE: the app version number in the Chart.yaml is the tag for the *container image*, which has the
                    # dots of the git tag replaced with dashes; replacing the first two dashes should get us back the
                    # original SemVer version number
                    app_version = parsed['appVersion'].replace('-', '.', 2)
                    existing = chart_map.get(app_version)
                    if existing is not None:
                        if semver.compare(existing['name'], h['name']) > 0:
                            # existing tag in the map is already greater (i.e. newer) than the current one -> skip
                            continue
                    chart_map[app_version] = h

            for tag in tags:
                tag_name = tag['name']
                if semver_pattern.match(tag_name):
                    logger.debug(f"Name = '{tag_name}', CommitID = {tag['commit']['id']}")
                    helm_tag = chart_map.get(tag_name)
                    build_index(service, tag, helm_chart, helm_tag)
                else:
                    logger.warning(f"Not a valid semver: Tag '{tag_name}' for service '{service['name']}'")

    cscf['lastModified'] = datetime.now(timezone.utc).isoformat()
    with open(os.path.join(output_folder, "index.json"), "w") as out:
        json.dump(cscf, out, indent=2)

    # self-check: ensure the created CSCF matches the schema
    schema_ref = {"$ref": cscf_schema_url}
    jsonschema.validate(cscf, schema_ref)

    upload_index()


main()
